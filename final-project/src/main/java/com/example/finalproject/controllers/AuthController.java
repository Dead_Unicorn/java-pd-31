package com.example.finalproject.controllers;

import com.example.finalproject.config.JwtTokenUtil;
import com.example.finalproject.dto.auth.UserDto;
import com.example.finalproject.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthService authService;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> authenticateUser(@RequestBody UserDto userDto ) throws Exception{
        authService.authenticateUser(userDto.toDomain());

        final UserDetails userDetails = authService.loadUserByUsername(userDto.getUserName());

        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(token);
    }

    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDto userDto) throws Exception {
        UUID createdId = authService.SaveNew(userDto.toDomain());
        return ResponseEntity.ok(createdId);
    }
    
}
