package com.example.finalproject.controllers;

import com.example.finalproject.services.rights.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.message.AuthException;

@RestController
@RequestMapping("/api/v1/rights")
public class RightsController {
    
    @Autowired
    private SectionService sectionService;
    
    @RequestMapping("sections")
    public ResponseEntity<?> getSections(@RequestHeader("Authorization") String token) throws AuthException {
        var sections = sectionService.getSectionsForUser(token);
        return ResponseEntity.ok(sections);//todo протести шо если два хедера кинуть
    }
    
}
