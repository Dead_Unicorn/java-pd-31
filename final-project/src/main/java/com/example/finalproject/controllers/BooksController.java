package com.example.finalproject.controllers;

import com.example.finalproject.config.AuthenticationRequired;
import com.example.finalproject.domain.books.Mapper;
import com.example.finalproject.dto.books.BookDto;
import com.example.finalproject.services.books.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/books")
@CrossOrigin
public class BooksController {
    
    @Autowired
    private BooksService booksService;
    
    @RequestMapping(value = "get-all", method = RequestMethod.GET)
    public ResponseEntity<?> getAllBooks(){
        var booksDto = booksService.getAllBooks().stream().map(Mapper::dtoToDomain).collect(Collectors.toList());
        return ResponseEntity.ok(booksDto);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @AuthenticationRequired(role = "Admin")
    public ResponseEntity<?> saveBooks(@RequestBody BookDto book){
        var createdId = booksService.saveBook(Mapper.dtoToDomain(book));
        return ResponseEntity.ok(createdId);
    }

    @RequestMapping(value = "del", method = RequestMethod.DELETE)
    @AuthenticationRequired(role = "Admin")
    public ResponseEntity<?> deleteBookById(@RequestParam UUID id){
        var deletedId = booksService.deleteBook(id);
        return ResponseEntity.ok(deletedId);
    }
    
}
