package com.example.finalproject.domain.user;

public enum UserRole {
    User,
    Admin
}
