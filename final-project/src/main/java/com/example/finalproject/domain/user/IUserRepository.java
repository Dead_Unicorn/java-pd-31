package com.example.finalproject.domain.user;

import com.example.finalproject.dal.user.UserDal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IUserRepository extends CrudRepository<UserDal, UUID> {
    @Nullable
    UserDal findByUsername(String username);
}
