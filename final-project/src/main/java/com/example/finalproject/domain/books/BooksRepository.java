package com.example.finalproject.domain.books;

import com.example.finalproject.dal.books.BookDal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BooksRepository extends CrudRepository<BookDal, UUID> {
    List<BookDal> findByName(String name);
    List<BookDal> findByAuthor(String author);
    List<BookDal> findByPrice(double price);
}
