package com.example.finalproject.domain.books;

import java.util.UUID;

public class BookModel {
    private UUID id;
    private String name;
    private String author;
    private String description;
    private double price;

    public BookModel(UUID id, String name, String author, String description, double price) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }
}
