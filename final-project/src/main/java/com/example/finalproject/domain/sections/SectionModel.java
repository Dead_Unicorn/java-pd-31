package com.example.finalproject.domain.sections;

import java.util.UUID;

public class SectionModel {
    private UUID id;
    private String name;
    private String path;


    public SectionModel(UUID id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
    
}
