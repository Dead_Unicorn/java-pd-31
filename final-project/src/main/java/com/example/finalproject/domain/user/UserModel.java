package com.example.finalproject.domain.user;

import com.example.finalproject.dal.user.UserDal;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.UUID;

public class UserModel {

    private UUID id;
    private String username;
    private String password;
    private UserRole role;

    public UserModel(UUID id, String username, String password, UserRole role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    

    public UUID getId() {
        return id;
    }

    public UserRole getRole() {
        return role;
    }

    public UserModel secure(PasswordEncoder bcryptEncoder){
        this.password = bcryptEncoder.encode(this.password);
        return this;
    }
    
    public UserDal toDal(){
        var userDal = new UserDal();
        userDal.setUsername(this.username);
        userDal.setPassword(this.password);
        userDal.setRole(this.role);
        return userDal;
    }
    
}
