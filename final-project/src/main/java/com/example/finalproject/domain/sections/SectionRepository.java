package com.example.finalproject.domain.sections;

import com.example.finalproject.dal.section.SysSection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SectionRepository extends CrudRepository<SysSection, UUID> {
}
