package com.example.finalproject.domain.books;

import com.example.finalproject.dal.books.BookDal;
import com.example.finalproject.dto.books.BookDto;

public class Mapper {
    public static BookModel dalToDomain(BookDal bookDal){
        return new BookModel(
                bookDal.getId(), 
                bookDal.getName(),
                bookDal.getAuthor(),
                bookDal.getDescription(),
                bookDal.getPrice()
        );
    }

    public static BookDal domainToDal(BookModel bookModel){
        BookDal bookDal = new BookDal();
        bookDal.setId(bookModel.getId());
        bookDal.setName(bookModel.getName());
        bookDal.setAuthor(bookModel.getAuthor());
        bookDal.setDescription(bookModel.getDescription());
        bookDal.setPrice(bookModel.getPrice());
        return bookDal;
    }
    
    public static BookDto dtoToDomain(BookModel bookModel){
        return new BookDto(
                bookModel.getId(),
                bookModel.getName(),
                bookModel.getAuthor(),
                bookModel.getDescription(),
                bookModel.getPrice()
        );
    }

    public static BookModel dtoToDomain(BookDto bookDto){
        return new BookModel(
                bookDto.getId(),
                bookDto.getName(),
                bookDto.getAuthor(),
                bookDto.getDescription(),
                bookDto.getPrice()
        );
    }
    
}
