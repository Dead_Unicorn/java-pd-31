package com.example.finalproject.dto.rights;

import java.util.UUID;

public class SectionDto {
    private UUID id;
    private String name;
    private String path;


    public SectionDto(UUID id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
