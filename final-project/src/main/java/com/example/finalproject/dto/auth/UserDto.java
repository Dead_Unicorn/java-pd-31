package com.example.finalproject.dto.auth;

import com.example.finalproject.domain.user.UserModel;
import com.example.finalproject.domain.user.UserRole;

import java.util.UUID;

public class UserDto {
    private UUID id;
    private String username;
    private String password;
    private UserRole role = UserRole.User; 
    
    public UserDto(UUID id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
//        this.role = UserRole.User;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public UserModel toDomain(){
        return new UserModel(id, username, password, role);
    }
    
}
