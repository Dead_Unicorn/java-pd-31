package com.example.finalproject.services;

import com.example.finalproject.dal.user.UserDal;
import com.example.finalproject.domain.user.IUserRepository;
import com.example.finalproject.domain.user.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;
    
    
    
    public void authenticateUser(UserModel user) throws Exception {
        var userModel = userRepository.findByUsername(user.getUsername()).toDomain();
        if(!bcryptEncoder.matches(user.getPassword(), userModel.getPassword())) {
            throw new Exception();
        }
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>() {{
                    add(new SimpleGrantedAuthority(user.getRole().name()));
                }});
    }

    public UUID SaveNew(UserModel user){
        return userRepository.save(
                user.secure(bcryptEncoder).toDal()
        ).getId();
    }
    
    
}
