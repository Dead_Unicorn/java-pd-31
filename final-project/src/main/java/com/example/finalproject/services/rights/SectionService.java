package com.example.finalproject.services.rights;

import com.example.finalproject.config.JwtTokenUtil;
import com.example.finalproject.dal.section.SysSection;
import com.example.finalproject.domain.sections.SectionModel;
import com.example.finalproject.domain.sections.SectionRepository;
import com.example.finalproject.domain.user.UserRole;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.security.auth.message.AuthException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SectionService {
    
    @Autowired
    private SectionRepository sectionRepository;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    public List<SectionModel> getSectionsForUser(String token) throws AuthException {
        String stringRole = ((Claims)jwtTokenUtil.validateAndReturn(token).getBody()).get("Role").toString();
        int roleValue = (int)UserRole.valueOf(stringRole).ordinal();
        return ((List<SysSection>)sectionRepository.findAll())
                .stream()
                .filter(x->x.getMinimalRight() <= roleValue)
                .map(SysSection::toDomain)
                .collect(Collectors.toList());
    }
    
}
