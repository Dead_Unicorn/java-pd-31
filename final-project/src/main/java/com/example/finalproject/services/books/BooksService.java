package com.example.finalproject.services.books;

import com.example.finalproject.dal.books.BookDal;
import com.example.finalproject.domain.books.BookModel;
import com.example.finalproject.domain.books.BooksRepository;
import com.example.finalproject.domain.books.Mapper;
import com.fasterxml.jackson.databind.deser.std.UUIDDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BooksService {
    @Autowired
    private BooksRepository booksRepository;
    
    public List<BookModel> getAllBooks(){
        return ((List<BookDal>)booksRepository.findAll()).stream().map(Mapper::dalToDomain).collect(Collectors.toList());   
    }
    
    public UUID saveBook(BookModel book){
        return booksRepository.save(Mapper.domainToDal(book)).getId();
    }

    public UUID deleteBook(UUID targetId){
        booksRepository.deleteById(targetId);
        return targetId;
    }

}
