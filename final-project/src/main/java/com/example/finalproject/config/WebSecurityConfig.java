package com.example.finalproject.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.security.cert.X509Certificate;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService userService;

//    @Autowired
//    private JwtRequestFilter jwtRequestFilter;

//    @Autowired
//    private AuthenticationFilter authenticationFilter;
//    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() throws Exception {
//        var encoderType = SecurityUtil.class.getDeclaredField("encoder").getAnnotatedType();
//        return (PasswordEncoder)encoderType.getClass().newInstance();
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity.csrf().disable().cors().and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests().anyRequest().permitAll();
//        httpSecurity.csrf().disable()
//                .cors().and()
//                // dont authenticate this particular request
//                .authorizeRequests().antMatchers("/api/v1/auth/**").permitAll()
//                .and()
////                .authorizeRequests()
////                    .antMatchers("/","/index.html", "/favicon.ico", "/img/**", "/js/**", "/front/**", "/css/**")
////                    .permitAll()
////                .and()
//                .authorizeRequests()
//                .antMatchers("/api/v1/books/add", "/api/v1/books/del").authenticated()
//                .and()
//                .authorizeRequests()
//                // all other requests need to be authenticated
//                .antMatchers("/api/v1/**")
//                .authenticated()
//                .and()
//                .authorizeRequests()
//                // all other requests need to be authenticated
//                .anyRequest()
//                .permitAll()
//                .and()
//                // make sure we use stateless session; session won't be used to
//                // store user's state.
//                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                        .and().formLogin();
//
//        // Add a filter to validate the tokens with every request
//        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
//        httpSecurity.addFilterAfter(authenticationFilter, JwtRequestFilter.class);
    }
    
    
    
}
