package com.example.finalproject.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.example.finalproject.domain.user.UserRole;
import io.jsonwebtoken.Jwt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.security.auth.message.AuthException;

@Component
public class JwtTokenUtil implements Serializable {
    public final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;//todo @valeu

    @Value("${jwt.secret}")
    private  String secret;

    public  String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public  String getRoleFromToken(String token) {
        return getClaimFromToken(token, x->x.get("role")).toString();
    }
    
    public   Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public  <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private  Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private  Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public  String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        var role = userDetails.getAuthorities().stream().findFirst().get();
        claims.put("role", UserRole.valueOf(role.getAuthority()).ordinal());
        return doGenerateToken(claims, userDetails.getUsername());
    }

    private  String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public  Boolean validateToken(String token, UserDetails userDetails) {
        validateSignature(token);
        final String username = getUsernameFromToken(token);
        return !isTokenExpired(token);
    }

    public Jwt validateAndReturn(String token) throws AuthException {
        Jwt jwt = validateSignature(token);
        final String username = getUsernameFromToken(token);
        if (isTokenExpired(token)) {
            throw new AuthException("Jwt invalid");
        }
        return jwt;
    }
    
    private  Jwt validateSignature(String token){
        return Jwts.parser().setSigningKey(secret).parse(token);
    }
    
}
