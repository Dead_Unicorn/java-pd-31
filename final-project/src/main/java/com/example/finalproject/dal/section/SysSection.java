package com.example.finalproject.dal.section;

import com.example.finalproject.domain.sections.SectionModel;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "SysSections")
public class SysSection {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    
    @Column(unique = true)
    private String path;

    @Column(unique = true)
    private String name;

    @Column
    private int minimalRight;

    
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimalRight() {
        return minimalRight;
    }

    public void setMinimalRight(int minimalRight) {
        this.minimalRight = minimalRight;
    }

    public SectionModel toDomain(){
        return new SectionModel(this.id, this.name, this.path);
    }
    
}
