package com.example.finalproject.dal.books;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Books")
public class BookDal {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String author;
    private String description;
    private double price;
    
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
