package com.example.finalproject.dal.user;

import com.example.finalproject.domain.user.UserModel;
import com.example.finalproject.domain.user.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.boot.web.embedded.netty.NettyWebServer;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name ="users")
public class UserDal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    
    @Column(unique = true)
    private String username;
    
    @Column
    @JsonIgnore
    private String password;

    private UserRole role;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   
    public UUID getId(){
        return id;
    }
    
    public UserModel toDomain(){
        return new UserModel(id, username, password, role);
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
