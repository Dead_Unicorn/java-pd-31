export default interface Token{
    sub: string,
    role: Role, 
    iat: number,
    exp: number,
}

export enum Role{
    nonInit = -1,
    user,
    admin
}