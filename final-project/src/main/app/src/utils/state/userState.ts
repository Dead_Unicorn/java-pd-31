import constants from "@/utils/constants/constants";
import jwtDecode from "jwt-decode";
import Token, {Role} from "@/utils/state/token";

export default class UserState {
    
    private static _instance : UserState;
    private readonly _username: string;
    private readonly _role: Role;
    private _isInited = false;
    
    get role(): Role {
        return this._role;
    }
    get username(): string {
        return this._username;
    }
    get isInited(): boolean {
        return this._isInited;
    }
    private constructor() {
        const token : string|null = localStorage.getItem(constants.tokenKey);
        console.log(token)
        if (!token){
            console.log(token)
            this._role = Role.nonInit;
            this._username = "";
            return;
        }
        const decoded = jwtDecode<Token>(token, {header : false});
        this._role = decoded.role;
        this._username = decoded.sub;
        this._isInited = true;
    }
    
    public static getInstance(){
        if (!UserState._instance){
            UserState._instance = new UserState()
        }
        
        return UserState._instance;
    }
    
    public static setup(){
        if (UserState.getInstance()._isInited)
            return;
        UserState._instance = new UserState()
        UserState._instance._isInited = true;
        console.log(UserState._instance)
    }
    
    
} 

// export interface UserState{
//     username: string,
//     role: number,
//     //todo mb sessionduration or whatever
// }






