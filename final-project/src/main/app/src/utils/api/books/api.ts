import client from "@/utils/api/client";
import Book from "@/utils/api/books/book";

const baseUrl = "books/";

export function getAllBooks(){
    const url = "get-all";
    return client.get(baseUrl+url);
}

export function saveBook(payload: Book){
    const url = "add";
    return client.post(baseUrl+url, payload);
}

export function deleteBookById(id: string){
    const url = "del/";
    return client.delete(baseUrl + url + id);
}
