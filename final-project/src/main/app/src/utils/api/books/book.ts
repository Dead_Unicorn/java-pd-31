export default interface Book{
    id: string, 
    name: string, 
    author: string, 
    description: string, 
    price: number
}

export interface Books{
    books: Book[]
}