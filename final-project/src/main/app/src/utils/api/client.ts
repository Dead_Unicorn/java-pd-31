import axios from 'axios';
import constants from "@/utils/constants/constants";



const client = axios.create({
    baseURL : constants.baseUrl+constants.apiUrl, 
    headers: { "Content-Type":"application/json",}
})

client.interceptors.request.use(ful => {
    const token = localStorage.getItem(constants.tokenKey);

    if (token && ful.headers){
        ful.headers["Authorization"] = `Bearer ${token}`;
    }
    return ful;
}, rej => {
    console.log(rej)
    return Promise.reject(rej);
})

export default client;