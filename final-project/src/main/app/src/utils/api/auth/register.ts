import {LoginData} from "@/utils/api/auth/models/loginData";
import client from "@/utils/api/client";


const url = "auth/register";
export function register(payload : LoginData){
    return client.post(url, payload);
}