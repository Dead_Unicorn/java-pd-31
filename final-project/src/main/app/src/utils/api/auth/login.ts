import client from "@/utils/api/client";
import {LoginData} from "@/utils/api/auth/models/loginData"; 


export function login(payload: LoginData){
    const url = "auth/login";
    return client.post(url, payload);
}
