import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import constants from "@/utils/constants/constants";
import {login} from "@/utils/api/auth/login";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path:"/login",
    name:"Login",
    component: () => import("@/views/Login.vue"),
  },
  {
    path:"/register",
    name:"Register",
    component: () => import("@/views/Register.vue"),
  },
  {
    path:"/books",
    name:"Books",
    component: () => import("@/views/Books.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),//process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) =>{
  if (to.path == '/'){
    router.push('/login');
  }
  const token = localStorage.getItem(constants.tokenKey);
  if (!token && (to.path != "/login" && to.path != "/register" && to.path != "about")){
    router.push('/login');
  }
  // if ((to.path == '/login' || to.path == '/register') && token){
  //   router.push('/books');
  // }
  next();
});



export default router;
